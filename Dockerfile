# base image
FROM python:3.8-alpine

# workdir
WORKDIR /app

# copy requir file
COPY requirements.txt .

# install requirements
RUN pip install -r requirements.txt

# copy code for workdir
COPY src/ .

# command to run on container start
CMD [ "flask", "run", "--host=0.0.0.0" ]